import {Pipe, PipeTransform} from "@angular/core";

import {Product} from "../models/product";

@Pipe({
    name:"OrderPrice"
})

export class OrderPricePipe implements PipeTransform{

    transform(products:Product[],order:string):Product[]{

        let orderedProducts:Product[];

        if(order==="asc"){

            orderedProducts= products.sort((productA:Product,productB:Product):number =>{
                return productA.price > productB.price?1:productA.price<productB.price?-1:0;
            });
        }
        else{
            orderedProducts= products.sort((productA:Product,productB:Product):number =>{
                return productB.price > productA.price?1:productB.price<productA.price?-1:0;
            });
        }
        return orderedProducts;
    }


}
