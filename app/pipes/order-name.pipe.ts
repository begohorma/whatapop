import {Pipe, PipeTransform} from "@angular/core";

import {Product} from "../models/product";

@Pipe({
    name:"OrderName"
})

export class OrderNamePipe implements PipeTransform{

    transform(products:Product[],order:string):Product[]{

        let orderedProducts:Product[];

        if(order==="asc"){

            orderedProducts= products.sort((productA:Product,productB:Product):number =>{
                return productA.name.toLowerCase() > productB.name.toLowerCase() ? 1 : productA.name.toLowerCase() < productB.name.toLowerCase() ? -1 : 0;
            });
        }
        else{
            orderedProducts= products.sort((productA:Product,productB:Product):number =>{
                return productB.name.toLowerCase() > productA.name.toLowerCase() ? 1 : productB.name.toLowerCase() < productA.name.toLowerCase() ? -1 : 0;
            });
        }
        return orderedProducts;
    }


}
