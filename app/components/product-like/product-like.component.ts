import {Component, OnInit,Input} from "@angular/core";


@Component({
    selector:"like-icon",
    templateUrl:"./app/components/product-like/product-like.component.html",
    styleUrls: ["./app/components/product-like/product-like.component.css"]
})

export class ProductLikeComponent implements OnInit{

    @Input() productId:number;


    likeStatus:string="unliked";

    ngOnInit():void{
        //Al cargar el template comprueba si el usuario ha guardaro el productId
        //Si es así es que lo ha marcado como que le  gusta por lo que likeStatus
        //debe ser liked
        if(typeof(localStorage) !== "undefined"){
           if(localStorage.getItem(this.productId.toString())){
                this.likeStatus="liked";
            }
        }
    }

    changeLikeStatus():void{
        //cuando se pulsa sobre el icono de like hay que cambiar el estado al contrario del que tenía
        this.likeStatus=this.likeStatus ==="unliked"?"liked":"unliked";
        //si el nuevo estado es liked hay que añadir el productId al local Storage
        if (this.likeStatus==="liked")
        {
            if(typeof(localStorage) !== "undefined"){
                localStorage.setItem(this.productId.toString(),"liked");
            }
        }
        else{
            //Si el estado es unliked eliminar el productId del localStorage
            if(typeof(localStorage) !== "undefined"){
                localStorage.removeItem(this.productId.toString());
            }
        }

    }

}
